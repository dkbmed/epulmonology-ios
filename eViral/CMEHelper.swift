//
//  CMEHelper.swift
//  eViral
//
//  Created by Abhinay on 22/03/18.
//  Copyright © 2018 ONS Application Studio. All rights reserved.
//

import Foundation

struct CME
{
    let url:String!
    let title:String!
    
    init(dict:[String:String]){
        self.title = dict["title"]
        self.url = dict["url"]
    }
}

//Class
final class CMEHelper
{
    typealias CMECompletionHandler = (_ requestSuccess:Bool, _ message:String) -> ()
    static let sharedInstance = CMEHelper()
    
    var record:CME!
}

extension CMEHelper
{
    //MARK:- API calling
    static func getLink(completionHandler:@escaping CMECompletionHandler)
    {
        let requestURL = EViralCostant.API.CMEInfo
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: nil, encoding: .URL) { (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                if let responseValue = value as? [String:String]
                {
                    CMEHelper.sharedInstance.record = CME(dict: responseValue)
                    completionHandler(true, EViralCostant.Alert.Success)
                }else{
                    completionHandler(false, EViralCostant.Alert.SomethingWentWrong)
                }
                break
            case .SomethingWrong:
                completionHandler(false, EViralCostant.Alert.SomethingWentWrong)
                break
            }
        }
    }
    
}
