//
//  SortVC.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/1/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

protocol SortVCDelegate:class{
    func sortApplied(sortSection:SortSection)
}


class SortVC: UIViewController
{
    //MARK:- Public Var
    weak var delegate:SortVCDelegate?
    var previousSelectedSortSection = SortSection.None
    
    //MARK:- Private Var
    fileprivate var dataSource = SortHelper.dataSource
    
    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var viewContainer: UIView!
    
    //MARK:- ViewC LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()
        pageAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addScreenTracking(forControllerName: "Sort Page")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        print("SortVC Deinit")
    }
    
    //MARK:- Private Methods
    fileprivate func initialSetting()
    {
        tableView.delegate = self
        tableView.dataSource = self
        SortCell.registerNib(tableView: tableView)
        
    }
    
    fileprivate func pageAppearance(){
        viewContainer.drawBorder(cornerRadius: 5.0, borderWidth: 0.0, borderColor: ClearColor, maskToBound: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    fileprivate func refresh()
    {
        //dataSource = CategoryHelper.sharedInstance.allCategories
        
        //filterParams.removeAll()
        previousSelectedSortSection = .None
        tableView.reloadData()
    }
    
    //MARK:- IBAction Methods
    @IBAction func applyAction(_ sender: Any)
    {
        delegate?.sortApplied(sortSection: previousSelectedSortSection)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func claerAction(_ sender: Any) {
        refresh()
    }

}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension SortVC
{
    struct Storyboard {
        static let ControllerId = "SortVC"
        static let SegueId = "SortSegue"
    }
    
    static func instantiate() -> SortVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerId) as! SortVC
    }
}

extension SortVC:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: SortCell.Constant.Identifier) as! SortCell
        cell.tag = indexPath.row
        cell.delegate = self
        cell.isRowSelected = false
        
        if previousSelectedSortSection != .None{
            let previousRow = previousSelectedSortSection.rawValue
            if previousRow == indexPath.row{
                cell.isRowSelected = true
            }
        }
        
        
        return cell
    }
}

extension SortVC:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SortCell.Constant.Height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! SortCell).data = dataSource[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath) as! SortCell
        cell.isRowSelected = !cell.isRowSelected
        self.isSortRowSelected(index: indexPath.row, flag: cell.isRowSelected)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SortVC:SortCellDelegate
{
    func isSortRowSelected(index: Int, flag:Bool)
    {
        if flag {
            previousSelectedSortSection = SortSection(rawValue: index)!
        }else{
            previousSelectedSortSection = .None
        }
        
        self.tableView.reloadData()
    }
}

