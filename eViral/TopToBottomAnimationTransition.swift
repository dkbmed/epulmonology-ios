//
//  TopToBottomAnimationTransition.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/2/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

class TopToBottomAnimationTransition: NSObject {

}
//MARK:- Extension
extension TopToBottomAnimationTransition{
    struct Animation {
        static let Duration = BlurPopUpAnimationController.Animation.Duration
    }
}

extension TopToBottomAnimationTransition:UIViewControllerAnimatedTransitioning
{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Animation.Duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        guard let fromViewController = transitionContext.viewController(forKey: .from), let toViewController = transitionContext.viewController(forKey: .to) else{
            return
        }
        
        //This view acts as a superview for the views which involves in Transition
        let containerView = transitionContext.containerView
        containerView.addSubview(toViewController.view)
        containerView.sendSubview(toBack: toViewController.view)
        
        let snapshotView = fromViewController.view.snapshotView(afterScreenUpdates: false)
        snapshotView?.frame = fromViewController.view.frame
        containerView.addSubview(snapshotView!)
        let bounds = UIScreen.main.bounds
        
        fromViewController.view.removeFromSuperview()
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations:
            {
                snapshotView?.frame = CGRect(x: 0, y: bounds.size.height, width: bounds.size.width, height: bounds.size.height)
                toViewController.view.alpha = 1.0
        },
                       completion: {
                        finished in
                        snapshotView?.removeFromSuperview()
                        transitionContext.completeTransition(true)
        })
        
    }
}
