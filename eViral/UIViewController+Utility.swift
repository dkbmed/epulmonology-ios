//
//  UIViewController+Utility.swift
//
//  Copyright © 2018 ONS. All rights reserved.
//

import Foundation

extension UIViewController
{
    func addScreenTracking(forControllerName name:String)
    {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: name)
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
}


