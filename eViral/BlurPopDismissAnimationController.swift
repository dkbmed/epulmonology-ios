//
//  BlurPopDismissAnimationController.swift
//  Interview
//
//  Created by Abhinay Maurya on 13/10/16.
//  Copyright © 2016 ONS. All rights reserved.
//

import UIKit

class BlurPopDismissAnimationController:NSObject
{
    
}


//MARK:- Extension
extension BlurPopDismissAnimationController{
    struct Animation {
        static let Duration = BlurPopUpAnimationController.Animation.Duration
    }
}

extension BlurPopDismissAnimationController:UIViewControllerAnimatedTransitioning
{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Animation.Duration
    }
             
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        guard let fromViewController = transitionContext.viewController(forKey: .from), let toViewController = transitionContext.viewController(forKey: .to) else{
            return
        }
        
        //This view acts as a superview for the views which involves in Transition
        let containerView = transitionContext.containerView
        containerView.addSubview(toViewController.view)
        containerView.sendSubview(toBack: toViewController.view)
        
        let snapshotView = fromViewController.view.snapshotView(afterScreenUpdates: false)
        snapshotView?.frame = fromViewController.view.frame
        containerView.addSubview(snapshotView!)
        
        
        fromViewController.view.removeFromSuperview()
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations:
            {
                snapshotView?.frame = fromViewController.view.frame.insetBy(dx: fromViewController.view.frame.size.width / 2, dy: fromViewController.view.frame.size.height / 2)
                toViewController.view.alpha = 1.0
            },
            completion: {
                finished in
                snapshotView?.removeFromSuperview()
                transitionContext.completeTransition(true)
        })
        
    }
}
