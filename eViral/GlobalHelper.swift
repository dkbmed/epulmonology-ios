//
//  GlobalHElper.swift
//  DayXDay
//
//  Created by Abhinay Maurya on 18/10/16.
//  Copyright © 2016 Faiqtalat. All rights reserved.
//

import UIKit


let WhiteColor = UIColor.white
let BlackColor = UIColor.black
let ClearColor = UIColor.clear
let DarkGray   = UIColor.darkGray
let Gray       = UIColor.gray
let LightGray  = UIColor.lightGray
