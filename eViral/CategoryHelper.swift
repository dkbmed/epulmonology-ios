//
//  CategoryHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/1/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation

struct Category
{
    let id:Int!
    let name:String!
    let diseaseId:Int!
    var isSelected = false
    
    init(category:[String:Any])
    {
        self.id = category["ID"] as! Int
        self.name = category["Category"] as! String
        self.diseaseId = category["Disease"] as! Int
    }
}

struct ArticleCategory
{
    let name:String!
    let articles:[Article]!
    
    init (name:String, articleInCategory:[[String:Any]]){
        self.name = name
        var articles = [Article]()
        for article in articleInCategory
        {
            let record = Article(article: article)
            if record.is_published != 0{
                articles.append(record)
            }else{
                let userEmail = UserHelper.getUserEmail()
                if userEmail == EViralCostant.Login.adminEmail{
                    articles.append(record)
                }
            }
        }
        self.articles = articles
    }
}

final class CategoryHelper
{
    typealias CategoryCompletionHandler = (_ requestSuccess:Bool)  -> ()
    static let sharedInstance = CategoryHelper()
    
    var allCategories:[Category]{
        get{
            return dataSource
        }
    }
    
    fileprivate var dataSource = [Category]()
    
    var articlesInCategory:[ArticleCategory]{
        get{
            return articleCategoryDataSource
        }
    }
    
    fileprivate var articleCategoryDataSource = [ArticleCategory]()
}

extension CategoryHelper
{
    func getAllCategories(completionHandler:@escaping CategoryCompletionHandler)
    {
        let requestURL = EViralCostant.API.Categories
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: nil, encoding: .URL) {[weak self] (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                
                if let records = value as? [[String:Any]]
                {
                    self?.dataSource.removeAll()
                    
                    for record in records{
                        let category = Category(category: record)
                        self?.dataSource.append(category)
                    }
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
                break
            case .SomethingWrong:
                completionHandler(false)
                break
            }
        }
    }
    
    func getArticlesInCategories(completionHandler:@escaping CategoryCompletionHandler)
    {
        let requestURL = EViralCostant.API.ArticlesInCategory
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: nil, encoding: .URL) {[weak self] (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                
                if let records = value as? [String:[[String:Any]]]
                {
                    self?.articleCategoryDataSource.removeAll()
                    
                    let keys = records.keys
                    
                    for key in keys{
                        if let record = records[key]{
                            let articleCategory = ArticleCategory(name: key, articleInCategory: record)
                            self?.articleCategoryDataSource.append(articleCategory)
                        }
                        
                    }
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
                break
            case .SomethingWrong:
                completionHandler(false)
                break
            }
        }
    }
        
}

