//
//  EViralCostant.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/25/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

struct EViralCostant
{
    static let AppName = "ePlumonology"
    
    struct Color {
        static let RedColor = UIColor(red: 234.0/255.0, green: 109.0/255.0, blue: 82.0/255.0, alpha: 1.0)
        static let SkyColor = UIColor(red: 225.0/255.0, green: 245.0/255.0, blue: 254/255.0, alpha: 1.0)
        static let GrayColor = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 241.0/255.0, alpha: 1.0)
        static let BlueColor = UIColor(red: 0.0/255.0, green: 4.0/255.0, blue: 65.0/255.0, alpha: 1.0)
        static let YellowColor = UIColor(red: 235.0/255.0, green: 194.0/255.0, blue: 70.0/255.0, alpha: 1.0)

    }
    
    struct Font
    {
        static let Verdana = "Verdana"
    }
    
    struct Storyboard {
        static let Main = "Main"
        static let Home = "Home"
    }
    
    struct Alert {
        static let Success = "Success!!"
        static let Error = "Error!!"
        static let Fail = "Failed!!"
        static let Warning = "Warning!!"
        static let Wait = "Please wait!!"
        static let Ooooops = "Ooooops!!"
        static let SomethingWentWrong = "Something went wrong!!"
        static let InternetNotComing = "The internet connection appears to be offline. You will need internet connection to access some links and videos"
    }
    
    struct Notification {
        
    }
    
    struct Image {
        static let CompressQuality:CGFloat = 0.40
    }
    
    struct API {
        //
        //static let Base            = "http://dkbmed.com/dkb-journal/public/"
        static let Base             = "http://epulm.eliteraturereview.org/public/"
        static let BaseAPI         = Base + "api/"
        static let Login           = BaseAPI + "login?"
        static let Register        = BaseAPI + "register?"
        static let Specialty       = BaseAPI + "specialties"
        static let Profession      = BaseAPI + "professions"
        //static let Articles        = BaseAPI + "articles"
        static let Articles        = BaseAPI + "article/user/id/"
        static let ProgressInfo    = BaseAPI + "article/progress/set-progress-info"
        static let Thumb           = Base + "article_thumbs/"
        static let KeywordSearch   = BaseAPI + "article/search/keywords"
        static let TitleSearch     = BaseAPI + "article/search/title"
        static let Categories      = BaseAPI + "category/all"
        static let ArticlesInCategory = BaseAPI + "article/search/category"
        static let LastSevenDaysArticle = BaseAPI + "article/released"
        static let EmailConstant = "?email="
        
        static let AboutUs = BaseAPI + "cms/aboutus"
        static let CMEInfo = BaseAPI + "cms/cme"
        static let Faculty = BaseAPI + "cms/faculty"
        
    }
    
    struct Login {
        static let adminEmail = "info@dkbmd.com"
    }
    
    //static let OneSignalId = "a858a67d-dbf7-4f96-b661-a121ca9863b8"
    static let OneSignalId = "1b367700-7e48-41e4-b465-1fe30e50a584"
    static let GoogleAnalyticsID = "UA-28804977-22"
}
