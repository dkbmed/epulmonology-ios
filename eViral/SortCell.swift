//
//  SortCell.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/1/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

protocol SortCellDelegate:class {
    func isSortRowSelected(index:Int, flag:Bool)
}


class SortCell: UITableViewCell
{
    var data:String!{
        didSet{
            lblTitle.text = data
        }
    }
    var isRowSelected = false{
        didSet{
            self.didClickOnCheckBox()
        }
    }
    
    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var btnSelection: UIButton!
    
    //MARK:- Private Var
    
    weak var delegate:SortCellDelegate?
    
    //MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.didClickOnCheckBox()
    }

    //MARK:- Other
    fileprivate func didClickOnCheckBox()
    {
        if isRowSelected{
            let image = UIImage(named: "checkbox")
            btnSelection.setBackgroundImage(image, for: .normal)
        }else{
            let image = UIImage(named: "crop-square")
            btnSelection.setBackgroundImage(image, for: .normal)
        }
    }
    
    //MARK:- IBAction
    @IBAction fileprivate func checkboxAction(sender:UIButton)
    {
        isRowSelected = !isRowSelected
        self.didClickOnCheckBox()
        delegate?.isSortRowSelected(index: self.tag, flag: isRowSelected)
    }
    
}
/* ---------------------------------- Extension ----------------------------------------- */
//MARK:- Extension

extension SortCell
{
    struct Constant {
        static let Identifier = "SortCell"
        static let Height:CGFloat = 44.0
    }
    
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: Constant.Identifier, bundle: nil), forCellReuseIdentifier: Constant.Identifier)
    }
}
