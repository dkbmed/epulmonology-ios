//
//  ArticleHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/27/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation
import Alamofire

struct Article
{
    let id:Int!
    let title:String!
    let p_description:String!
    let article_path:String!
    var article_type = ""
    var article_category = ""
    let is_published:Int!
    let thumb:String!
    var keywords = ""
    let created_on:String!
    let email_required:Bool!
    var updated_at = ""
   
    init(article:[String:Any])
    {
        self.id = article["id"] as! Int
        self.created_on = article["created_at"] as! String
        self.title = article["title"] as! String
        self.p_description = article["description"] as! String
        self.article_path = article["article_path"] as! String
        self.is_published = article["is_published"] as! Int
        self.thumb = article["thumb"] as! String
        self.email_required = article["email_required"] as! Bool
        
        if let updated_at = article["updated_at"] as? String{
            self.updated_at = updated_at
        }
        if let article_type = article["article_type"] as? String{
            self.article_type = article_type
        }
        if let article_category = article["article_category"] as? String{
            self.article_category = article_category
        }
        if let keywords = article["keywords"] as? String{
            self.keywords = keywords
        }
    }
}

final class ArticleHelper
{
    typealias ArticleCompletionHandler = (_ requestSuccess:Bool) -> ()
    static let sharedInstance = ArticleHelper()
    
    var progressRecords:[UserProgressRecord]{
        get{
            return userProgressRecords
        }
    }
    fileprivate var userProgressRecords = [UserProgressRecord]()
    
    var dataSource:[Article]{
        get{
            return articles
        }
    }
    fileprivate var articles = [Article]()
}

extension ArticleHelper
{
    //MARK:- API calling
    func getArticles(completionHandler:@escaping ArticleCompletionHandler)
    {
        let requestURLString = EViralCostant.API.Articles
        let requestURL = requestURLString + UserHelper.getUserId()
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: nil , encoding: .URL) {[weak self] (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                
                if let info = value as? [String:[[String:Any]]]
                {
                    self?.userProgressRecords.removeAll()
                    self?.articles.removeAll()
                    
                    if let records = info["articles"]{
                        for article in records{
                            let record = Article(article: article)
                            
                            if record.is_published != 0{
                                self?.articles.append(record)
                            }else{
                                let userEmail = UserHelper.getUserEmail()
                                if userEmail == EViralCostant.Login.adminEmail{
                                    self?.articles.append(record)
                                }
                            }
                            
                        }
                    }
                    
                    if let userProgressRecords = info["user_progress"]{
                        for userProgress in userProgressRecords{
                            let record = UserProgressRecord(userProgress: userProgress)
                            self?.userProgressRecords.append(record)
                        }
                    }
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
                break
            case .SomethingWrong:
                completionHandler(false)
                break
            }
        }
    }
    
    
    
}

extension ArticleHelper
{
    //Save file on local
    static func saveFile(fileName:String, urlString:String, completionHandler:@escaping ArticleCompletionHandler)
    {
        print(urlString)
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(fileName)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlString, to: destination).response { response in
            print(response)
            
            if let _ = response.destinationURL?.path {
                completionHandler(true)
            }else{
                completionHandler(false)
            }
        }
    }
    
    //File saved or not
    static func isFileSavedOnLocal(fileName:String) -> Bool
    {
        let documentsURL = try! FileManager().url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: true)
        
        let url = documentsURL.appendingPathComponent(fileName)
        return FileManager().fileExists(atPath: url.path)
    }
}

extension ArticleHelper{
    
    func updateProgressInfoForArticleId(articleId:Int, progress:String)
    {
        var index = 0
        var progressRecord:UserProgressRecord?
        
        for record in userProgressRecords
        {
            let a_id = record.articleId
            if a_id == articleId{
                progressRecord = record
                progressRecord?.progressInfo = progress
                break
            }
            index += 1
        }
        
        if let record = progressRecord{
            userProgressRecords[index] = record
        }
    }
    
}

//SORT Extension
extension ArticleHelper
{
    static func articleStartedButDidNotCompleted (categoriesToFilter:[Category]) -> [Articles]
    {
        var sortDataSource = [Articles]()
        if let records = ArticleCoreHelper.getAllRecords()
        {
            var dataSource = [Articles]()
            
            //First apply filter
            if categoriesToFilter.count > 0
            {
                for record in records
                {
                    if let arrCategories = record.a_category?.components(separatedBy: ",")
                    {
                        var isRecordAdded = false
                        for category in categoriesToFilter
                        {
                            for categoryId in arrCategories{
                                if categoryId == String(category.id) {
                                    dataSource.append(record)
                                    isRecordAdded = true
                                    break
                                }
                            }
                            if isRecordAdded{
                                break
                            }
                        }
                    }
                }
            }else{
                dataSource = records
            }
            
            //Second Apply Sort
            if let userProgressRecord = UserProgressCoreHelper.getAllRecords()
            {
                for record in dataSource{
                    for pRecord in userProgressRecord{
                        if pRecord.articleId == record.a_id{
                            if let progress = pRecord.progress{
                                if let progressVal = Double(progress){
                                    if progressVal < 100.0{
                                        sortDataSource.append(record)
                                        break
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
        }
        return sortDataSource
    }
    
    
    
    static func articleCompleted (categoriesToFilter:[Category]) -> [Articles]
    {
        var sortDataSource = [Articles]()
        if let records = ArticleCoreHelper.getAllRecords()
        {
            var dataSource = [Articles]()
            
            //First apply filter
            if categoriesToFilter.count > 0
            {
                for record in records
                {
                    if let arrCategories = record.a_category?.components(separatedBy: ",")
                    {
                        var isRecordAdded = false
                        for category in categoriesToFilter
                        {
                            for categoryId in arrCategories{
                                if categoryId == String(category.id) {
                                    dataSource.append(record)
                                    isRecordAdded = true
                                    break
                                }
                            }
                            if isRecordAdded{
                                break
                            }
                        }
                    }
                }
            }else{
                dataSource = records
            }
            
            //Second Apply Sort
            if let userProgressRecord = UserProgressCoreHelper.getAllRecords()
            {
                for record in dataSource{
                    for pRecord in userProgressRecord{
                        if pRecord.articleId == record.a_id{
                            if let progress = pRecord.progress{
                                if let progressVal = Double(progress){
                                    if progressVal == 100.0{
                                        sortDataSource.append(record)
                                        break
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
        }
        return sortDataSource
    }
    
    
    static func articleNeverViewed (categoriesToFilter:[Category]) -> [Articles]
    {
        var sortDataSource = [Articles]()
        if let records = ArticleCoreHelper.getAllRecords()
        {
            var dataSource = [Articles]()
            
            //First apply filter
            if categoriesToFilter.count > 0
            {
                for record in records
                {
                    if let arrCategories = record.a_category?.components(separatedBy: ",")
                    {
                        var isRecordAdded = false
                        for category in categoriesToFilter
                        {
                            for categoryId in arrCategories{
                                if categoryId == String(category.id) {
                                    dataSource.append(record)
                                    isRecordAdded = true
                                    break
                                }
                            }
                            if isRecordAdded{
                                break
                            }
                        }
                    }
                }
            }else{
                dataSource = records
            }
            
            //Second Apply Sort
            if let userProgressRecord = UserProgressCoreHelper.getAllRecords()
            {
                var index = 0
                for record in dataSource
                {
                    for pRecord in userProgressRecord{
                        if pRecord.articleId == record.a_id{
                            dataSource.remove(at: index)
                            index -= 1
                            break
                        }
                    }
                    index += 1
                }
                sortDataSource = dataSource
            }
        }
        return sortDataSource
    }
}

//Filter Extension
extension ArticleHelper
{
    static func getArticles_OnlyAppliedFilter (categoriesToFilter:[Category]) -> [Articles]
    {
        var dataSource = [Articles]()
        if let records = ArticleCoreHelper.getAllRecords()
        {
            //First apply filter
            if categoriesToFilter.count > 0
            {
                for record in records
                {
                    if let arrCategories = record.a_category?.components(separatedBy: ",")
                    {
                        var isRecordAdded = false
                        for category in categoriesToFilter
                        {
                            for categoryId in arrCategories{
                                if categoryId == String(category.id) {
                                    dataSource.append(record)
                                    isRecordAdded = true
                                    break
                                }
                            }
                            if isRecordAdded{
                                break
                            }
                        }
                    }
                }
            }else{
                dataSource = records
            }
        }
        return dataSource
    }
}
