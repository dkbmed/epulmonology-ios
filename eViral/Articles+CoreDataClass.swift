//
//  Articles+CoreDataClass.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/31/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation
import CoreData

@objc(Articles)
public class Articles: NSManagedObject {
    static let entityName = "Articles"

}
