//
//  UserProgress+CoreDataClass.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 10/30/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//
//

import Foundation
import CoreData

@objc(UserProgress)
public class UserProgress: NSManagedObject {
    static let entityName = "UserProgress"
}
