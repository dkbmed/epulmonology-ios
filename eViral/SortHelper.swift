//
//  SortHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/1/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation

enum SortSection:Int {
    case LastSevenDays = 0, DidNotCompleted, NeverViewed, Completed, None
}

final class SortHelper
{
    typealias SortCompletionHandler = (_ requestSuccess:Bool)  -> ()
    
    
    static let sharedInstance = SortHelper()
    
    static let dataSource = ["Articles released in last 7 days",
                             "Articles started but didn't completed yet",
                             "Articles that never viewed",
                             "Articles completed"]
    
    var lastSevenDaysArticle:[Article]{
        get{
            return lastSevenDaysArticleDataSource
        }
    }
    fileprivate var lastSevenDaysArticleDataSource = [Article]()
}

extension SortHelper
{
    static func getLastSevenDaysArticle(completionHandler:@escaping SortCompletionHandler)
    {
        let requestURL = EViralCostant.API.LastSevenDaysArticle
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: nil, encoding: .URL) { (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                
                if let records = value as? [[String:Any]]
                {
                    var articles = [Article]()
                    for article in records{
                        let record = Article(article: article)
                        if record.is_published != 0{
                            articles.append(record)
                        }else{
                            let userEmail = UserHelper.getUserEmail()
                            if userEmail == EViralCostant.Login.adminEmail{
                                articles.append(record)
                            }
                        }
                    }
                    completionHandler(true)
                }else{
                    completionHandler(false)
                }
                
                break
            case .SomethingWrong:
                completionHandler(false)
                break
            }
        }
    }
}

extension SortHelper
{
   
}


