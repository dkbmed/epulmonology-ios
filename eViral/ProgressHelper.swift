//
//  ProgressHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 10/30/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation

struct UserProgressRecord
{
    let id:Int!
    let userId:Int!
    let articleId:Int!
    let isComplete:Int!
    var progressInfo:String!
    let lastOpen:String!
    let createdAt:String!
    let updatedAt:String!
    
    init(userProgress:[String:Any])
    {
        self.id = userProgress["id"] as! Int
        self.userId = userProgress["user_id"] as! Int
        self.articleId = userProgress["article_id"] as! Int
        self.isComplete = userProgress["is_complete"] as! Int
        
        self.createdAt = userProgress["created_at"] as! String
        
        if let updated_at = userProgress["updated_at"] as? String{
            self.updatedAt = updated_at
        }else{
            self.updatedAt = ""
        }
        
        if let last_open = userProgress["last_open"] as? String{
            self.lastOpen = last_open
        }else{
            self.lastOpen = ""
        }
       
        if let progress_info = userProgress["progress_info"] as? String{
            self.progressInfo = progress_info
        }else{
           self.progressInfo = ""
        }
    }
    
    
    
}


final class ProgressHelper
{
    typealias ProgressCompletionHandler = (_ requestSuccess:Bool) -> ()
    
    static let sharedInstance = ProgressHelper()
    
    var selecedWebPageProgress:Double = 1.0
}
extension ProgressHelper
{
    //MARK:- API calling
    static func setProgressInfoForArticleId(articleId:String, progressInfo:String, completionHandler:@escaping ProgressCompletionHandler)
    {
        let requestURL = EViralCostant.API.ProgressInfo
        let params = ["article_id":articleId,
                      "user_id":UserHelper.getUserId(),
                      "progress_info":progressInfo]
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: params as [String : AnyObject], encoding: .URL) { (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                
                if let responseDict = value as? [String:Any]{
                    let status = responseDict["status"] as! Int
                    if status == 1{
                        completionHandler(true)
                    }else{
                        completionHandler(false)
                    }
                    
                }
                break
            case .SomethingWrong:
                completionHandler(false)
                break
            }
        }
    }
    
    static func updateProgressInfoForArticleId(articleId:Int, progress:String)
    {
        ArticleHelper.sharedInstance.updateProgressInfoForArticleId(articleId: articleId, progress: progress)
    }
    
}
