//
//  Articles+CoreDataProperties.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/31/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation
import CoreData


extension Articles {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Articles> {
        return NSFetchRequest<Articles>(entityName: "Articles")
    }

    @NSManaged public var a_id: Int64
    @NSManaged public var a_title: String?
    @NSManaged public var a_description: String?
    @NSManaged public var a_path: String?
    @NSManaged public var a_thumb: String?
    @NSManaged public var a_createdOn: String?
    @NSManaged public var a_image: NSData?
    
    @NSManaged public var a_isPublished: Int64
    @NSManaged public var a_category: String?
    @NSManaged public var a_keywords: String?
    @NSManaged public var a_type: String?
    @NSManaged public var a_updatedAt: String?
    
    @NSManaged public var a_isEmailRequired:Bool

}
