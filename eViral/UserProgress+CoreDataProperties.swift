//
//  UserProgress+CoreDataProperties.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 10/30/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//
//

import Foundation
import CoreData


extension UserProgress {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserProgress> {
        return NSFetchRequest<UserProgress>(entityName: "UserProgress")
    }

    @NSManaged public var articleId: Int64
    @NSManaged public var isCompleted: Int64
    @NSManaged public var lastOpen: String?
    @NSManaged public var progress: String?

}
