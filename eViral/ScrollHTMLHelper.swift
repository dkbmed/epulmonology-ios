//
//  ScrollHTMLHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 9/28/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation
final class ScrollHTMLHelper
{
    static func getCurrentOffsetYForFile(htmlFile:String) -> Double
    {
        let prefs = UserDefaults.standard
        return prefs.double(forKey:htmlFile)
    }
    static func setCurrentOffsetYForFile(htmlFile:String, yOffset:Double)
    {
        let prefs = UserDefaults.standard
        prefs.set(yOffset, forKey: htmlFile)
        prefs.synchronize()
    }
}
