//
//  FacultyVC.swift
//  eViral
//
//  Created by Abhinay on 12/03/18.
//  Copyright © 2018 ONS Application Studio. All rights reserved.
//

import UIKit

class FacultyVC: UIViewController
{

    
    //MARK:-IBOutlet
    @IBOutlet fileprivate weak var webView: UIWebView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:-Private Var
    fileprivate var isFirstTime = true
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageAppearance()
        self.initialSetting()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addScreenTracking(forControllerName: "Faculty Page")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    fileprivate func pageAppearance(){
        self.title = "Faculty"
    }
    
    fileprivate func initialSetting()
    {
        webView.delegate = self
        webView.scalesPageToFit = true
        self.loadData()
    }
    
    fileprivate func loadData()
    {
        self.showLoadIndicator(title: "Loading..")
        FacultyHelper.getLink {[weak self] (status, message) in
            self?.hideLoadIndicator()
            if status{
                self?.loadWebView()
            }
        }
    }
    
    fileprivate func loadWebView()
    {
        let url = URL(string: FacultyHelper.sharedInstance.record.url)!
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
    }
    
    //MARK:- IBAction
    @IBAction func openLeftMenu(){
        self.slideMenuController()?.openLeft()
    }
    
    
}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension FacultyVC
{
    struct Storyboard {
        static let ControllerID = "FacultyVC"
    }
    
    static func instantiate() -> FacultyVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerID) as! FacultyVC
    }
}

extension FacultyVC:UIWebViewDelegate
{
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.stopAnimating()
    }
}




