//
//  FacultyHelper.swift
//  eViral
//
//  Created by Abhinay on 22/03/18.
//  Copyright © 2018 ONS Application Studio. All rights reserved.
//

import Foundation
struct Faculty
{
    let url:String!
    let title:String!
    
    init(dict:[String:String]){
        self.title = dict["title"]
        self.url = dict["url"]
    }
}

//Class
final class FacultyHelper
{
    typealias FacultyCompletionHandler = (_ requestSuccess:Bool, _ message:String) -> ()
    static let sharedInstance = FacultyHelper()
    
    var record:Faculty!
}

extension FacultyHelper
{
    //MARK:- API calling
    static func getLink(completionHandler:@escaping FacultyCompletionHandler)
    {
        let requestURL = EViralCostant.API.Faculty
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: nil, encoding: .URL) { (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                if let responseValue = value as? [String:String]
                {
                    FacultyHelper.sharedInstance.record = Faculty(dict: responseValue)
                    completionHandler(true, EViralCostant.Alert.Success)
                }else{
                    completionHandler(false, EViralCostant.Alert.SomethingWentWrong)
                }
                break
            case .SomethingWrong:
                completionHandler(false, EViralCostant.Alert.SomethingWentWrong)
                break
            }
        }
    }
    
}
