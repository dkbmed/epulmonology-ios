//
//  ArticleCell.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/28/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit
import Kingfisher
import UICircularProgressRing

class ArticleCell: UITableViewCell
{

    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var lblDescription: UILabel!
    @IBOutlet fileprivate weak var imgArticle: UIImageView!
    @IBOutlet fileprivate weak var activityIndicator:   UIActivityIndicatorView!
    @IBOutlet fileprivate weak var progressView: UICircularProgressRingView!
    
    
    //MARK:- Public Vars
    var data:Articles?{
        didSet{
            lblTitle.text = data?.a_title
            lblDescription.text = data?.a_description
            
            if let bgImageURL = data?.a_thumb
            {
                let urlString = EViralCostant.API.Thumb + bgImageURL
                let url = URL(string: urlString)!
                
                self.activityIndicator.startAnimating()
                imgArticle.kf.setImage(with: url, placeholder: UIImage(named: "noImage"), options: nil, progressBlock: nil, completionHandler: {[weak self] (ctype) in
                    self?.activityIndicator.stopAnimating()
                })
                
            }
            
        }
    }
    
    var progress:CGFloat!{
        didSet{
            progressView.value = progress
        }
    }
    
    
    //MARK:- View Life Cycle
    
    //MARK:-
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.ringStyle = .gradient
    }

    //MARK:- Private Methods
    
    //MARK:- Public Methods
    func setData (article:Article){
        lblTitle.text = article.title
        lblDescription.text = article.p_description
    }
    
}


/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension
extension ArticleCell
{
    struct Constant {
        static let Identifier = String(describing: ArticleCell.self)
        static let Height:CGFloat = 125.0
    }
    
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: Constant.Identifier, bundle: nil), forCellReuseIdentifier: Constant.Identifier)
    }
    
}





