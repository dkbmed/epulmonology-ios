//
//  WebReaderVC.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/28/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit
import Reachability
import MediaPlayer

class WebReaderVC: UIViewController
{
    //MARK:-IBOutlet
    @IBOutlet fileprivate weak var webView: UIWebView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var backButton: UIBarButtonItem!
    @IBOutlet fileprivate weak var refreshButton: UIBarButtonItem!
    @IBOutlet fileprivate weak var forwardButton: UIBarButtonItem!
    @IBOutlet fileprivate weak var stopButton: UIBarButtonItem!
    
    //MARK:-Private Var
    fileprivate var isWebsiteShown = false
    fileprivate let reachability = Reachability()
    
    //MARK:-Public Var
    var article:Articles!
    var localUrl:String?
    
    var progressCompleted:CGFloat = 0.0
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageAppearance()
        self.initialSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addScreenTracking(forControllerName: "WebReader")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    fileprivate func pageAppearance(){
        self.title = article.a_title
    }
    
    fileprivate func initialSetting()
    {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
            // report for an error
        }
        
        webView.delegate = self
        webView.scrollView.delegate = self
        webView.scalesPageToFit = true
        webView.isUserInteractionEnabled = false
        stopButton.isEnabled = false
        
        //declare this property where it won't go out of scope relative to your listener
        
        
        reachability?.whenReachable = {[weak self] reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                if reachability.isReachableViaWiFi {
                    self?.openWebsite()
                } else {
                    self?.openWebsite()
                }
            }
        }
        reachability?.whenUnreachable = {[weak self] reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                self?.opneLocalFile()
            }
        }
        
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    fileprivate func opneLocalFile()
    {
        if isWebsiteShown{
            return
        }
        let url = URL(fileURLWithPath: localUrl!)
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
    
    fileprivate func openWebsite()
    {
        
        if let uRLString = article.a_path
        {
            isWebsiteShown = true
            if article.a_isEmailRequired
            {
                let mailId = UserHelper.getUserEmail()
                let fullUrl = "\(uRLString)\(EViralCostant.API.EmailConstant)\(mailId)"
                webView.loadRequest(URLRequest(url: URL(string: fullUrl)!))
            }else{
                webView.loadRequest(URLRequest(url: URL(string: uRLString)!))
            }
        }
    }
    
    fileprivate func updateButtons () {
        forwardButton.isEnabled = self.webView.canGoForward;
        backButton.isEnabled = self.webView.canGoBack;
    }
    
    fileprivate func setWebPageContnetOffsetAccordingToUserProgress()
    {
        let totalHeight = self.webView.scrollView.contentSize.height
        var contentOffsetY = (totalHeight * progressCompleted) / 100
        if progressCompleted == 100.0{
            contentOffsetY -= self.webView.bounds.size.height
        }
        
        let contentOffset = CGPoint(x: self.webView.scrollView.contentOffset.x, y: contentOffsetY)
        self.webView.scrollView.setContentOffset(contentOffset, animated: true)
    }
    
    
    //MARK:- IBAction Methods
    @IBAction fileprivate func backAction(sender: AnyObject){
        webView.goBack()
        self.updateButtons()
    }
    
    @IBAction fileprivate func refreshAction(sender: AnyObject){
        webView.reload()
        self.updateButtons()
    }
    
    @IBAction fileprivate func stopAction(sender: AnyObject){
        webView.stopLoading()
    }
    
    @IBAction fileprivate func fowardAction(sender: AnyObject){
        webView.goForward()
        self.updateButtons()
    }


}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension WebReaderVC
{
    struct Storyboard {
        static let ControllerID = "WebReaderVC"
        static let SegueIdentifier = "WebReaderSegue"
    }
    
    static func instantiate() -> WebReaderVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerID) as! WebReaderVC
    }
}

extension WebReaderVC:UIWebViewDelegate
{
    func webViewDidStartLoad(_ webView: UIWebView) {
        stopButton.isEnabled = true
        refreshButton.isEnabled = false
        self.updateButtons()
        
        
        activityIndicator.isHidden = false;
        activityIndicator.startAnimating()
        self.webView.isUserInteractionEnabled = false
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        stopButton.isEnabled = false
        refreshButton.isEnabled = true
        self.updateButtons()
        
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true;
        self.webView.isUserInteractionEnabled = true
        
        self.setWebPageContnetOffsetAccordingToUserProgress()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        stopButton.isEnabled = false
        refreshButton.isEnabled = true
        self.updateButtons()
        
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true;
        self.webView.isUserInteractionEnabled = true
    }
}

extension WebReaderVC:UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if self.webView.isUserInteractionEnabled{
            if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height){
                ProgressHelper.sharedInstance.selecedWebPageProgress = 100.0
            }
            else if scrollView.contentOffset.y <= 0.0{
                ProgressHelper.sharedInstance.selecedWebPageProgress = 0.0
            } else{
                let contentOffsetY = scrollView.contentOffset.y
                let totalHeight = scrollView.contentSize.height
                let progress = ((contentOffsetY * 100)/totalHeight)
                ProgressHelper.sharedInstance.selecedWebPageProgress = Double(progress)
            }
        }
        
            
    }
}
