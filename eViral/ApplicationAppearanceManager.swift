//
//  ApplicationAppearanceManager.swift
//
//  Created by Abhinay Maurya on 16/10/16.
//  Copyright © 2016 ONS. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

final class ApplicationAppearanceManager
{
    class func appLoadingAppearance()
    {
        SlideMenuOptions.contentViewScale = 1.0
        SlideMenuOptions.contentViewOpacity = 0.25
        SlideMenuOptions.hideStatusBar = false
        
        UINavigationBar.appearance().tintColor = BlackColor
        UINavigationBar.appearance().barTintColor = EViralCostant.Color.SkyColor
     //   UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 27),NSForegroundColorAttributeName:WhiteColor]
        UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: UIFont(name: "verdana", size: 24) ?? UIFont.systemFont(ofSize: 27),NSForegroundColorAttributeName:EViralCostant.Color.BlueColor]
        
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 17),NSForegroundColorAttributeName:DarkGray], for: .normal)
        
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: WhiteColor], for: .selected)
    }
    
    
    
}
