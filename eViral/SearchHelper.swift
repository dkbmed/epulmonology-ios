//
//  SearchHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 10/31/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation

final class SearchHelper{
    typealias SearchCompletionHandler = (_ requestSuccess:Bool, _ articles:[Article]?)  -> ()
}

extension SearchHelper
{
    static func searchByKeywords(keyword:String, completionHandler:@escaping SearchCompletionHandler)
    {
        let requestURL = EViralCostant.API.KeywordSearch
        let params = ["s":keyword]
        
        HTTPRequestManager.httpRequest(url: requestURL, protocolMethod: .GET, parameters: params as [String : AnyObject], encoding: .URL) { (response, value, statusCode) in
            
            switch response
            {
            case .Success:
                
                if let records = value as? [[String:Any]]
                {
                    var articles = [Article]()
                    for article in records{
                        let record = Article(article: article)
                        if record.is_published != 0{
                            articles.append(record)
                        }else{
                            let userEmail = UserHelper.getUserEmail()
                            if userEmail == EViralCostant.Login.adminEmail{
                                articles.append(record)
                            }
                        }
                    }
                    completionHandler(true, articles)
                }else{
                    completionHandler(false, nil)
                }
                
                break
            case .SomethingWrong:
                completionHandler(false, nil)
                break
            }
        }
    }
    
}
