//
//  CMEVC.swift
//  eViral
//
//  Created by Abhinay on 08/03/18.
//  Copyright © 2018 ONS Application Studio. All rights reserved.
//

import UIKit

class CMEVC: UIViewController
{
    //MARK:-IBOutlet
    @IBOutlet fileprivate weak var webView: UIWebView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:-Private Var
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageAppearance()
        self.initialSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addScreenTracking(forControllerName: "CME Page")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    fileprivate func pageAppearance(){
        self.title = "CME"
    }
    
    fileprivate func initialSetting()
    {
        webView.delegate = self
        webView.scalesPageToFit = true
        self.loadData()
    }
    
    fileprivate func loadData()
    {
        self.showLoadIndicator(title: "Loading..")
        CMEHelper.getLink {[weak self] (status, message) in
            self?.hideLoadIndicator()
            if status{
                self?.loadWebView()
            }
        }
    }
    
    fileprivate func loadWebView()
    {
        let url = URL(string: CMEHelper.sharedInstance.record.url)!
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
    }
    
    //MARK:- IBAction
    @IBAction func openLeftMenu(){
        self.slideMenuController()?.openLeft()
    }
    
    
}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension CMEVC
{
    struct Storyboard {
        static let ControllerID = "CMEVC"
    }
    
    static func instantiate() -> CMEVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerID) as! CMEVC
    }
}

extension CMEVC:UIWebViewDelegate
{
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.stopAnimating()
    }
}

