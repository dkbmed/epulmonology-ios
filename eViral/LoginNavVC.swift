//
//  LoginNavVC.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/25/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

class LoginNavVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Other
//    override var preferredStatusBarStyle: UIStatusBarStyle
//    {
//        return .lightContent
//    }

    
}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension LoginNavVC
{
    struct Storyboard {
        static let ControllerID = "LoginNavVC"
    }
    
    static func instantiate() -> LoginNavVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Main, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerID) as! LoginNavVC
    }
}

