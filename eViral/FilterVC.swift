//
//  FilterVC.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/1/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

protocol FilterVCDelegate:class{
    func filterApplied(records:[Category])
}


class FilterVC: UIViewController
{
    //MARK:- Public Var
    weak var delegate:FilterVCDelegate?
    var previousSelectedCategories = [Category]()
    
    //MARK:- Private Var
    fileprivate var dataSource = [Category]()
    fileprivate var filterParams = [Category]()
    
    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var viewContainer: UIView!
    
    //MARK:- ViewC LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()
        pageAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.addScreenTracking(forControllerName: "Filter")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit {
        print("FilterVC Deinit")
    }
    
    //MARK:- Private Methods
    fileprivate func initialSetting()
    {
        filterParams = previousSelectedCategories
        var index = 0
        for var record in filterParams{
            record.isSelected = true
            
            filterParams[index] = record
            index += 1
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        FilterCell.registerNib(tableView: tableView)
        lblTitle.text = "Filter by Category"
        
        //Fetch all categories
        if CategoryHelper.sharedInstance.allCategories.count == 0{
            self.showLoadIndicator(title: "Loading..")
            CategoryHelper.sharedInstance.getAllCategories(completionHandler: {[weak self] (status) in
                if status{
                    self?.dataSource = CategoryHelper.sharedInstance.allCategories
                }
                self?.hideLoadIndicator()
                self?.tableView.reloadData()
            })
        }else{
            refresh()
            if previousSelectedCategories.count > 0
            {
                var index = 0
                for var record in dataSource
                {
                    for selectedCategory in previousSelectedCategories{
                        if selectedCategory.id == record.id{
                            record.isSelected = true
                            dataSource[index] = record
                            break
                        }
                    }
                    index += 1
                }
                tableView.reloadData()
            }
        }
    }
    
    fileprivate func pageAppearance(){
        viewContainer.drawBorder(cornerRadius: 5.0, borderWidth: 0.0, borderColor: ClearColor, maskToBound: true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    fileprivate func refresh()
    {
        dataSource = CategoryHelper.sharedInstance.allCategories
        tableView.reloadData()
    }
    
    //MARK:- IBAction Methods
    
    @IBAction func claerAction(_ sender: Any) {
        filterParams.removeAll()
        refresh()
    }
    
    @IBAction func applyAction(_ sender: Any)
    {
        delegate?.filterApplied(records: filterParams)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension FilterVC
{
    struct Storyboard {
        static let ControllerId = "FilterVC"
        static let SegueId = "FilterSegue"
    }
    
    static func instantiate() -> FilterVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerId) as! FilterVC
    }
}

extension FilterVC:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterCell.Constant.Identifier) as! FilterCell
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
    }
}

extension FilterVC:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FilterCell.Constant.Height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        (cell as! FilterCell).data = dataSource[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let flag = !dataSource[indexPath.row].isSelected
        isFilterRowSelected(flag: flag, index: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadData()
    }
    
}

extension FilterVC:FilterCellDelegate
{
    func isFilterRowSelected(flag:Bool, index:Int)
    {
        let record = dataSource[index]
        dataSource[index].isSelected = flag
        
        if flag{
            filterParams.append(record)
        }else{
            filterParams = filterParams.filter {$0.name != record.name }
        }
    }
}
