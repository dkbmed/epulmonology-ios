//
//  UserProgressCoreHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 10/30/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation
import CoreData

class UserProgressCoreHelper
{
    enum Response : String{
        case RecordAdded,  SomethingWrong, RecordDeleted, RecordUpdated
    }
    
    static let entityName = UserProgress.entityName
    
    static func saveProgressRecord (record progress:UserProgressRecord) -> (Response)
    {
        
        let entity = NSEntityDescription.insertNewObject(forEntityName: UserProgressCoreHelper.entityName, into: CoreDataHelper.sharedInstance.managedObjectContex) as! UserProgress
        
        entity.isCompleted = Int64(progress.isComplete)
        entity.articleId = Int64(progress.articleId)
        
        entity.progress = progress.progressInfo
        entity.lastOpen = progress.lastOpen
        
        
        if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
            return  Response.RecordAdded
        }else{
            return Response.SomethingWrong
        }
    }
    
    static func updateRecordForArticleId(articleId:Int, progress:String) -> (Response)
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:UserProgress.entityName)
        let predicate = NSPredicate(format: "articleId == %d", articleId)
        fetchRequest.predicate = predicate
        
        guard let records = CoreDataHelper.sharedInstance.performFetch(fetchRequest) else {
            return Response.SomethingWrong
        }
        
        if records.count > 0 {
            let record = records[0] as! UserProgress
            record.progress = progress
            
            if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
                return Response.RecordUpdated
            }else{
                return Response.SomethingWrong
            }
        }else{
            let record = ["article_id":articleId,
                          "is_complete":0,
                          "progress_info":progress,
                          "last_open":Date().string(),
                          "created_at":Date().string(),
                          "updated_at":Date().string(),
                          "id":0,
                          "user_id":0
            
            ] as [String:Any]
            
            let progressRecord  = UserProgressRecord(userProgress: record)
            let status =  UserProgressCoreHelper.saveProgressRecord(record: progressRecord)
            
            if status == .RecordAdded{
                return Response.RecordUpdated
            }
        }
        
       return Response.SomethingWrong
    }
    
    static func recordExistOrNot (_ title:String) -> Bool
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:UserProgress.entityName)
        let predicate = NSPredicate(format: "articleId == %@", title)
        fetchRequest.predicate = predicate
        
        guard let records = CoreDataHelper.sharedInstance.performFetch(fetchRequest) else {
            return false
        }
        
        if records.count > 0 {
            return true
        }
        return false
    }
    
    
    static func deleteAllRecord() -> Bool
    {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: UserProgress.entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do{
            let _ = try CoreDataHelper.sharedInstance.managedObjectContex.execute(request)
            if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
                return  true
            }else{
                return false
            }
        }catch {
            return false
        }
        
        
    }
    
    //    static func deleteRecord(_ entity:Articles) -> (Response)
    //    {
    //        CoreDataHelper.sharedInstance.managedObjectContex.delete(entity)
    //
    //        if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
    //            return  (Response.RecordDeleted, nil)
    //        }else{
    //            return (Response.SomethingWrong, nil)
    //        }
    //    }
    
    static func getAllRecords() -> [UserProgress]?
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:UserProgress.entityName)
//        let sortDescriptor = NSSortDescriptor(key: "a_id", ascending: false)
//        fetchRequest.sortDescriptors = [sortDescriptor]
        
        guard let records = CoreDataHelper.sharedInstance.performFetch(fetchRequest) else {
            return nil
        }
        
        if records.count > 0 {
            return records as? [UserProgress]
        }
        return nil
    }
    
    
    
}
