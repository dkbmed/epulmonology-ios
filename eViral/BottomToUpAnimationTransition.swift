//
//  BottomToUpAnimationTransition.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/2/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

class BottomToUpAnimationTransition: NSObject {

}

//MARK:- Extension
extension BottomToUpAnimationTransition{
    struct Animation {
        static let Duration = 0.33
    }
}

extension BottomToUpAnimationTransition:UIViewControllerAnimatedTransitioning
{
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Animation.Duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        guard let fromViewController = transitionContext.viewController(forKey: .from), let toViewController = transitionContext.viewController(forKey: .to) else{
            return
        }
        
        let bounds = UIScreen.main.bounds
        let finalFrame = transitionContext.finalFrame(for: toViewController)
        //This view acts as a superview for the views which involves in Transition
        let containerView = transitionContext.containerView
        toViewController.view.frame = finalFrame
        
        containerView.addSubview(toViewController.view)
        containerView.sendSubview(toBack: toViewController.view)
        
        let snapshotView = toViewController.view.snapshotView(afterScreenUpdates: true)
        snapshotView?.frame = CGRect(x: 0, y: bounds.size.height, width: bounds.size.width, height: bounds.size.height)
        containerView.addSubview(snapshotView!)
        toViewController.view.alpha = 0
        
        
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromViewController.view.alpha = 0.50
            
            snapshotView?.frame = finalFrame
        }) { bool in
            //
            toViewController.view.alpha = 1
            snapshotView?.removeFromSuperview()
            transitionContext.completeTransition(true)
            
            let view = fromViewController.view
            UIApplication.shared.keyWindow?.addSubview(view!)
            UIApplication.shared.keyWindow?.sendSubview(toBack: view!)
        }
        
    }
}

