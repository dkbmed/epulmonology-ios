//
//  ArticleCoreHelper.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/31/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import Foundation
import CoreData

class ArticleCoreHelper
{
    
    enum Response : String{
        case RecordAdded,  SomethingWrong, RecordDeleted
    }
    
    static let entityName = Articles.entityName
    
    
    static func saveArticleRecord (record article:Article) -> (Response)
    {
        
        let entity = NSEntityDescription.insertNewObject(forEntityName: Articles.entityName, into: CoreDataHelper.sharedInstance.managedObjectContex) as! Articles
        entity.a_id = Int64(article.id)
        entity.a_title = article.title
        entity.a_path = article.article_path
        entity.a_thumb = article.thumb
        entity.a_createdOn = article.created_on
        entity.a_description = article.p_description
        
        entity.a_type = article.article_type
        entity.a_category = article.article_category
        entity.a_isPublished = Int64(article.is_published)
        entity.a_keywords = article.keywords
        entity.a_updatedAt = article.updated_at
        entity.a_isEmailRequired = article.email_required
        
        if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
            return  (Response.RecordAdded)
        }else{
            return (Response.SomethingWrong)
        }
    }
    
    static func recordExistOrNot (_ title:String) -> Bool
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:Articles.entityName)
        let predicate = NSPredicate(format: "a_title == %@", title)
        fetchRequest.predicate = predicate
        
        guard let records = CoreDataHelper.sharedInstance.performFetch(fetchRequest) else {
            return false
        }
        
        if records.count > 0 {
            return true
        }
        return false
    }
    
    
    static func deleteAllRecord() -> Bool
    {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: Articles.entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        do{
           let _ = try CoreDataHelper.sharedInstance.managedObjectContex.execute(request)
            if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
                return  true
            }else{
                return false
            }
        }catch {
            return false
        }
        
        
    }
    
//    static func deleteRecord(_ entity:Articles) -> (Response)
//    {
//        CoreDataHelper.sharedInstance.managedObjectContex.delete(entity)
//        
//        if CoreDataHelper.sharedInstance.syncContext() == CoreDataHelper.successKey {
//            return  (Response.RecordDeleted, nil)
//        }else{
//            return (Response.SomethingWrong, nil)
//        }
//    }
    
    static func getAllRecords() -> [Articles]?
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:Articles.entityName)
        let sortDescriptor = NSSortDescriptor(key: "a_id", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        guard let records = CoreDataHelper.sharedInstance.performFetch(fetchRequest) else {
            return nil
        }
        
        if records.count > 0 {
            return records as? [Articles]
        }
        return nil
    }
    
    
    
}
