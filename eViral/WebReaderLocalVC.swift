//
//  WebReaderLocalVC.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/29/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

class WebReaderLocalVC: UIViewController
{
    //MARK:-IBOutlet
    @IBOutlet fileprivate weak var webView: UIWebView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:-Public Var
    var filePath:String!
    var articleName:String!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageAppearance()
        self.initialSetting()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    fileprivate func pageAppearance(){
        self.title = articleName
    }
    
    fileprivate func initialSetting()
    {
        webView.delegate = self
        webView.scalesPageToFit = true
        
        let url = URL(fileURLWithPath: filePath)
        let request = URLRequest(url: url)
        webView.loadRequest(request)
        
        
       // let html = try? String(contentsOfFile: filePath, encoding: String.Encoding.utf8)
        //webView.loadHTMLString(html!, baseURL: nil)
        
    }
    
    //MARK:- IBAction
}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension WebReaderLocalVC
{
    struct Storyboard {
        static let ControllerID = "WebReaderLocalVC"
    }
    
    static func instantiate() -> WebReaderLocalVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerID) as! WebReaderLocalVC
    }
}

extension WebReaderLocalVC:UIWebViewDelegate
{
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.startAnimating()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.stopAnimating()
    }
}
