//
//  HomeVC.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 7/27/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit
import Reachability

class HomeVC: UIViewController
{
    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var tableView:UITableView!
    @IBOutlet fileprivate weak var interNetLabelBottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var btnClearWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var txtSearch: UITextField!
    @IBOutlet fileprivate weak var viewSearchContainer: UIView!
    
    //MARK:- Private Vars
    fileprivate var dataSource = [Articles]()
    fileprivate var allDataSource = [Articles]()
    
    fileprivate var categoriesToFilter = [Category]()
    
    fileprivate var progressDataSource = [UserProgress]()
    
    fileprivate var refreshControl: UIRefreshControl!
    fileprivate let reachability = Reachability()
    
    fileprivate var currentReaderDocument:ReaderDocument?
    fileprivate var selectedArticle:Articles!
    fileprivate var isWebPageOpened = false
    
    fileprivate var selectedSortSection = SortSection.None
    
    fileprivate var isSortVCSelected = false
    
    fileprivate let customPresentAnimationController = BlurPopUpAnimationController()
    fileprivate let customDismissAnimationController = BlurPopDismissAnimationController()
    
    fileprivate let customPresentAnimationController_ToTop = BottomToUpAnimationTransition()
    fileprivate let customPresentAnimationController_ToBottom = TopToBottomAnimationTransition()
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageAppearance()
        self.initialSetting()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Reachability_Custom.isInternetAvailable() == false{
            self.showAlert(title: "", message: EViralCostant.Alert.InternetNotComing)
        }
        
        if isWebPageOpened{
            let progressPercentage = String(ProgressHelper.sharedInstance.selecedWebPageProgress)
            self.setProgressForSelectedArticle(progressPercentage: progressPercentage)
            isWebPageOpened = false
        }
        self.addScreenTracking(forControllerName: "Home")
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if LocalNotificationHelper.showSixWeekReminderAlert()
        {
            self.showAlertWithOk(title: "Reminder!!", message: "Kindly enable the notification!!", completionHandler: { (status) in
                
                DispatchQueue.main.async {
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                LocalNotificationHelper.removeSixWeekReminder()
                            })
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Private Methods
    fileprivate func pageAppearance(){
        self.title = "ePulmonology review"
        navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        viewSearchContainer.drawBorder(cornerRadius: 1.0, borderWidth: 1.0, borderColor: Gray, maskToBound: true)
    }
    
    fileprivate func initialSetting()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: .UIApplicationWillEnterForeground, object: nil)

        btnClearWidthConstraint.constant = 0
        txtSearch.delegate = self
        
        self.navigationController?.navigationBar.isHidden = false
        ArticleCell.registerNib(tableView: tableView)
       
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(HomeVC.refresh), for: .valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        
        if Reachability_Custom.isInternetAvailable(){
            self.refresh()
        }else{
            if let records = ArticleCoreHelper.getAllRecords(){
                self.dataSource = records
            }
        }
        
        reachability?.whenReachable = {[weak self] reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                self?.interNetLabelBottomLayoutConstraint.constant = -20
            }
        }
        reachability?.whenUnreachable = {[weak self] reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                self?.interNetLabelBottomLayoutConstraint.constant = 0
            }
        }
        
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc fileprivate func refresh()
    {
        if Reachability_Custom.isInternetAvailable() == false{
            self.refreshControl.endRefreshing()
            self.showAlert(title: "", message: EViralCostant.Alert.InternetNotComing)
            return
        }
        if let searchText = txtSearch.text
        {
            if (searchText.count) > 0{
                didSelectOnSearch()
                return
            }
        }
        
        self.showLoadIndicator(title: "Loading..")
        ArticleHelper.sharedInstance.getArticles(completionHandler: {[weak self] (status) in
            self?.hideLoadIndicator()
            
            if status{
                
                let allArticles = ArticleHelper.sharedInstance.dataSource
                
                let status = ArticleCoreHelper.deleteAllRecord()
                
                if status == true
                {
                    for record in allArticles{
                       let _ =  ArticleCoreHelper.saveArticleRecord(record: record)
                    }
                }
                
                let allProgressRecords = ArticleHelper.sharedInstance.progressRecords
                let progressStatus = UserProgressCoreHelper.deleteAllRecord()
                
                if progressStatus == true
                {
                    for record in allProgressRecords{
                        let _ =  UserProgressCoreHelper.saveProgressRecord(record: record)
                    }
                }
                

                if let progressRecords = UserProgressCoreHelper.getAllRecords(){
                    self?.progressDataSource.removeAll()
                    self?.progressDataSource = progressRecords
                }
                
                if let records = ArticleCoreHelper.getAllRecords()
                {
                    self?.allDataSource.removeAll()
                    self?.dataSource.removeAll()
                    
                    self?.allDataSource = records
                    self?.dataSource = records
                    
                    self?.tableView.reloadData()
                    
                }
                self?.refreshControl.endRefreshing()
            }
        })
    }
    
    fileprivate func openPDFViewer(forFile fileId:String, articleName:String, index:Int)
    {
        let documentsURL = try! FileManager().url(for: .documentDirectory,
                                                  in: .userDomainMask,
                                                  appropriateFor: nil,
                                                  create: true)
        
        let url = documentsURL.appendingPathComponent(fileId)
        
        if url.pathExtension == "pdf" || url.pathExtension == "PDF"
        {
            currentReaderDocument = ReaderDocument(filePath: url.path, password: nil)
            currentReaderDocument?.pageNumber = NSNumber(value: self.getSelectedPDFCurrentPage())
            if (currentReaderDocument != nil) // Must have a valid ReaderDocument object in order to proceed with things
            {
                guard let vc  = ReaderViewController(readerDocument: currentReaderDocument!) else{
                    return
                }
                vc.delegate = self
                vc.title = articleName
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        }else{
            self.openWebPageForArticle(article:dataSource[index], url: url)
        }
    }
    
    fileprivate func openWebPageForArticle(article:Articles, url:URL)
    {
        isWebPageOpened = true
        
        let progressCompleted = self.getSelectedWebPageProgress()
        
        let vc = WebReaderVC.instantiate()
        vc.article = article
        vc.progressCompleted = progressCompleted
        vc.localUrl = url.path
        let _ = self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func refreshProgressRecord(articleId:Int, progressPercentage:String)
    {
        ProgressHelper.updateProgressInfoForArticleId(articleId: articleId, progress: progressPercentage)
        //Now update in core data
        let status = UserProgressCoreHelper.updateRecordForArticleId(articleId: articleId, progress: progressPercentage)
        
        if status == .RecordUpdated
        {
            if let progressRecords = UserProgressCoreHelper.getAllRecords(){
                self.progressDataSource.removeAll()
                self.progressDataSource = progressRecords
                tableView.reloadData()
            }
        }
    }
    
    fileprivate func getSelectedPDFCurrentPage() -> Int64
    {
        let articleId = selectedArticle.a_id
        for record in progressDataSource
        {
            if articleId == record.articleId{
                if let progressStr = record.progress{
                    if let progress  = Double(progressStr)
                    {
                        let document = currentReaderDocument!
                        let pageCount = Double(document.pageCount.intValue)
                        let currentPage = (pageCount * progress) / 100
                        return Int64(currentPage)
                    }
                }
                
                break
            }
        }
        
        return 1
    }
    
    
    fileprivate func getSelectedWebPageProgress() -> CGFloat
    {
        let articleId = selectedArticle.a_id
        for record in progressDataSource
        {
            if articleId == record.articleId{
                if let progressStr = record.progress
                {
                    if let progress  = Double(progressStr){
                        return CGFloat(progress)
                    }
                }
                
                break
            }
        }
        
        return 1.0
    }
    
    
    fileprivate func setProgressForSelectedArticle(progressPercentage:String)
    {
        let a_id = Int(selectedArticle.a_id)
        
        //Update on Server
        ProgressHelper.setProgressInfoForArticleId(articleId: String(a_id), progressInfo: progressPercentage) {[weak self] (status) in
            if status{
                //Update on Local
                self?.refreshProgressRecord(articleId: a_id, progressPercentage: progressPercentage)
            }
        }
    }
    
    fileprivate func didSelectOnSearch()
    {
        btnClearWidthConstraint.constant = 46.0
        
        self.showLoadIndicator(title: "Searching..")
        SearchHelper.searchByKeywords(keyword: txtSearch.text!, completionHandler: {[weak self] (status, articles) in
            self?.hideLoadIndicator()
            if status{
                let searchRecords = articles!
                self?.applySearchOnView(searchRecords: searchRecords)
            }
        })
    }
    
    fileprivate func applySearchOnView(searchRecords:[Article])
    {
        let allArticles = searchRecords
        let status = ArticleCoreHelper.deleteAllRecord()
        
        if status == true
        {
            for record in allArticles{
                let _ =  ArticleCoreHelper.saveArticleRecord(record: record)
            }
        }
        
        if let records = ArticleCoreHelper.getAllRecords()
        {
            self.allDataSource.removeAll()
            self.allDataSource = records
            self.dataSource.removeAll()
            self.dataSource = records
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    //MARK:- IBAction
    @IBAction func openLeftMenu(){
        self.slideMenuController()?.openLeft()
    }
    
    
    @IBAction func didClickOnClear(_ sender: UIButton)
    {
        txtSearch.text = ""
        btnClearWidthConstraint.constant = 0
        
        let allArticles = ArticleHelper.sharedInstance.dataSource
        let status = ArticleCoreHelper.deleteAllRecord()
        if status == true
        {
            for record in allArticles{
                let _ =  ArticleCoreHelper.saveArticleRecord(record: record)
            }
        }

        if let records = ArticleCoreHelper.getAllRecords()
        {
            self.dataSource.removeAll()
            self.dataSource = records
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    @IBAction func didClickOnFilter(_ sender: UIButton) {
        self.performSegue(withIdentifier: FilterVC.Storyboard.SegueId, sender: nil)
    }
    
    @IBAction func didClickOnSort(_ sender: UIButton) {
        self.performSegue(withIdentifier: SortVC.Storyboard.SegueId, sender: nil)
    }
    
    
    fileprivate func filterDataAccordingToSelectedCategory()
    {
        self.dataSource.removeAll()
        if categoriesToFilter.count == 0{
            self.dataSource = allDataSource
            tableView.reloadData()
            return
        }
        for record in allDataSource
        {
            if let arrCategories = record.a_category?.components(separatedBy: ",")
            {
                var isRecordAdded = false
                for category in self.categoriesToFilter
                {
                    for categoryId in arrCategories{
                        if categoryId == String(category.id) {
                            self.dataSource.append(record)
                            isRecordAdded = true
                            break
                        }
                    }
                    if isRecordAdded{
                        break
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    
    

    //MARK:- Other
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == WebReaderVC.Storyboard.SegueIdentifier{
            let index = sender as! Int
            let article = dataSource[index]
            let vc = segue.destination as! WebReaderVC
            vc.article = article
        }else if segue.identifier == FilterVC.Storyboard.SegueId
        {
            isSortVCSelected = false
            let vc = segue.destination as! FilterVC
            vc.transitioningDelegate = self
            vc.delegate = self
            vc.previousSelectedCategories = categoriesToFilter
        }
        else if segue.identifier == SortVC.Storyboard.SegueId
        {
            isSortVCSelected = true
            let vc = segue.destination as! SortVC
            vc.transitioningDelegate = self
            vc.delegate = self
            vc.previousSelectedSortSection = selectedSortSection
        }
    }
    
}

/* ---------------------------------- Extension --------------------------------------- */
//MARK:- Extension

extension HomeVC
{
    struct Storyboard {
        static let ControllerID = "HomeVC"
    }
    
    static func instantiate() -> HomeVC{
        let storyboard = UIStoryboard(name: EViralCostant.Storyboard.Home, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: Storyboard.ControllerID) as! HomeVC
    }
}

//MARK:- Extension UITableViewDataSource & Delegate
extension HomeVC:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.Constant.Identifier) as! ArticleCell
        return cell
    }
    
}

//MARK:- Extension Delegate
extension HomeVC:UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ArticleCell.Constant.Height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let article = dataSource[indexPath.row]
        (cell as! ArticleCell).data = article
        
        let articleId = article.a_id
        for record in progressDataSource{
            if articleId == record.articleId{
                if let progressStr = record.progress
                {
                    if let progress  = Double(progressStr){
                        (cell as! ArticleCell).progress = CGFloat(progress)
                    }
                }
                
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedArticle = dataSource[indexPath.row]
        
        let mailId = UserHelper.getUserEmail()
        var fileId = "\(selectedArticle.a_id)"
        var filePath = selectedArticle.a_path!
        
        
        
        let pathExtention = (filePath as NSString).pathExtension
        if pathExtention.count > 0{
             fileId.append(".\(pathExtention)")
        }
       
        if selectedArticle.a_isEmailRequired{
            filePath = "\(filePath)\(EViralCostant.API.EmailConstant)\(mailId)"
        }
        
        
        let isFileSavedOnLocal = ArticleHelper.isFileSavedOnLocal(fileName: fileId)
        
        if isFileSavedOnLocal
        {
            self.openPDFViewer(forFile: fileId, articleName: selectedArticle.a_title!, index: indexPath.row)
        }else{
            
            if Reachability_Custom.isInternetAvailable() == false{
                self.showAlert(title: EViralCostant.Alert.Ooooops, message: "You need to connect to internet to download this article")
                return
            }
            
            self.showLoadIndicator(title: "Processing..")
            ArticleHelper.saveFile(fileName: fileId, urlString: filePath, completionHandler: {[weak self] (status) in
                self?.hideLoadIndicator()
                self?.openPDFViewer(forFile: fileId, articleName: (self?.selectedArticle.a_title)!, index: indexPath.row)
            })
        }
        
    }

}

extension HomeVC:ReaderViewControllerDelegate
{
    func dismiss(_ viewController: ReaderViewController!)
    {
        if let pageCount = currentReaderDocument?.pageCount.intValue, let pageNumber = currentReaderDocument?.pageNumber.intValue
        {
            let pageCountInDouble = Double(pageCount)
            let progressPercentage =  (Double(pageNumber) * 100)/pageCountInDouble
            let progressPer = String(progressPercentage)
            self.setProgressForSelectedArticle(progressPercentage: progressPer)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension HomeVC:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if let searchText = textField.text{
            if (searchText.count) > 0{
                didSelectOnSearch()
            }
        }
        return true
    }
}

extension HomeVC:FilterVCDelegate
{
    func filterApplied(records: [Category])
    {
        categoriesToFilter.removeAll()
        categoriesToFilter = records
        
        var records = [Articles]()
        
        switch selectedSortSection
        {
            case .Completed:
                records = ArticleHelper.articleCompleted(categoriesToFilter: self.categoriesToFilter)
            case .DidNotCompleted:
                records = ArticleHelper.articleStartedButDidNotCompleted(categoriesToFilter: self.categoriesToFilter)
            case .NeverViewed:
                records = ArticleHelper.articleNeverViewed(categoriesToFilter: self.categoriesToFilter)
            
            case .None:
                records = ArticleHelper.getArticles_OnlyAppliedFilter(categoriesToFilter: self.categoriesToFilter)
        default:
            break
        }
        
        self.dataSource = records
        tableView.reloadData()
        
        //self.filterDataAccordingToSelectedCategory()
    }
}

extension HomeVC:SortVCDelegate
{
    func sortApplied(sortSection: SortSection)
    {
        self.selectedSortSection = sortSection
        var records = [Articles]()
        
        switch sortSection
        {
            case .Completed:
                records = ArticleHelper.articleCompleted(categoriesToFilter: self.categoriesToFilter)
            case .DidNotCompleted:
                records = ArticleHelper.articleStartedButDidNotCompleted(categoriesToFilter: self.categoriesToFilter)
            case .NeverViewed:
                records = ArticleHelper.articleNeverViewed(categoriesToFilter: self.categoriesToFilter)
        case .None:
            records = ArticleHelper.getArticles_OnlyAppliedFilter(categoriesToFilter: self.categoriesToFilter)
            
            default:
                break
        }
        
        self.dataSource = records
        tableView.reloadData()
    }
}

extension HomeVC:UIViewControllerTransitioningDelegate
{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        if isSortVCSelected{
            return customPresentAnimationController_ToTop
        }
        return customPresentAnimationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if isSortVCSelected{
            return customPresentAnimationController_ToBottom
        }
        return customDismissAnimationController
    }
}
