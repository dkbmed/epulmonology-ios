//
//  FilterCell.swift
//  DKBJournal
//
//  Created by Abhinay Maurya on 11/1/17.
//  Copyright © 2017 ONS Application Studio. All rights reserved.
//

import UIKit

protocol FilterCellDelegate:class {
    func isFilterRowSelected(flag:Bool, index:Int)
}

class FilterCell: UITableViewCell
{

    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var btnSelection: UIButton!
    
    //MARK:- Private Var
    fileprivate var isRowSelected = false
    
    //MARK:- Public Var
    weak var delegate:FilterCellDelegate?
    
    var data:Category!{
        didSet{
            lblTitle.text = data.name
            isRowSelected = data.isSelected
            
            if isRowSelected{
                let image = UIImage(named: "checkbox")
                btnSelection.setBackgroundImage(image, for: .normal)
            }else{
                let image = UIImage(named: "crop-square")
                btnSelection.setBackgroundImage(image, for: .normal)
            }
            
        }
    }
    
    //MARK:- IBAction
    @IBAction fileprivate func checkboxAction(sender:UIButton)
    {
        isRowSelected = !isRowSelected
        
        if isRowSelected{
            let image = UIImage(named: "checkbox")
            btnSelection.setBackgroundImage(image, for: .normal)
        }else{
            let image = UIImage(named: "crop-square")
            btnSelection.setBackgroundImage(image, for: .normal)
        }
        
        delegate?.isFilterRowSelected(flag: isRowSelected, index: self.tag)
    }
    
}
/* ---------------------------------- Extension ----------------------------------------- */
//MARK:- Extension

extension FilterCell
{
    struct Constant {
        static let Identifier = "FilterCell"
        static let Height:CGFloat = 44.0
    }
    
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: Constant.Identifier, bundle: nil), forCellReuseIdentifier: Constant.Identifier)
    }
}

